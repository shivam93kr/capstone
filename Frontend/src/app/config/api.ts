import { environment } from "src/environments/environment";

export const baseUrl = environment.production ? 'http://localhost:4200/' : 'http://localhost:9009/'
//'https://foodbox.cfapps.io/'
export const restaurantUrl = baseUrl + '/restaurant'
export const foodUrl = baseUrl + '/food'
export const userUrl = baseUrl + '/user'
export const cartUrl = baseUrl + '/cart'
export const orderUrl = baseUrl + '/order'